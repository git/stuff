package xyz.apiote.stuff

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.android.material.snackbar.Snackbar
import xyz.apiote.stuff.databinding.ActivityEditBinding

// todo consts

class EditActivity : AppCompatActivity() {
	private lateinit var binding: ActivityEditBinding
	private var id: Long? = null
	private var stuff = Stuff(null, "", "", -1)
	private var originalStuff = Stuff(null, "", "", -1)
	private lateinit var db: DB

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		binding = ActivityEditBinding.inflate(layoutInflater)
		setContentView(binding.root)

		val receivedContainer = intent.extras?.get("container") as Long?
		stuff.container = receivedContainer ?: -1

		binding.chooseContainer.setOnClickListener {
			val chooseIntent = Intent(this, ContainerActivity::class.java)
			chooseIntent.putExtra("ID", 0L)
			chooseIntent.putExtra("mode", "pick")
			startActivityForResult(chooseIntent, ContainerActivity.REQUEST.PICK.value)
		}

		db = DB(this)

		id = intent.extras?.get("ID") as Long?
		title = if (id != null) {
			stuff = db.loadStuff(id!!)
			binding.editName.setText(stuff.name)
			binding.editDescription.setText(stuff.description)
			binding.nameContainer.text = db.loadStuff(stuff.container).name
			getString(R.string.edit_stuff, stuff.name)
		} else {
			getString(R.string.add_stuff)
		}

		if (stuff.container != -1L) {
			binding.nameContainer.text = db.loadStuff(stuff.container).name
		} else {
			binding.nameContainer.text = getString(R.string.no_container)
		}

		originalStuff = stuff
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.menu_edit, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return when (item.itemId) {
			R.id.action_save -> {
				stuff.name = binding.editName.text.toString()
				stuff.description = binding.editDescription.text.toString()
				if (stuff.container == -1L) {
					Snackbar.make(
						binding.root,
						getString(R.string.container_must_not_be_empty),
						Snackbar.LENGTH_SHORT
					)
						.show()
					return true
				}
				if (stuff.name == "") {
					Snackbar.make(
						binding.root,
						getString(R.string.name_must_not_be_empty),
						Snackbar.LENGTH_SHORT
					)
						.show()
					return true
				}
				val (id, inserted) = db.saveStuff(stuff)
				val returnIntent = Intent()
				returnIntent.putExtra("stuffID", id as Long)
				returnIntent.putExtra("inserted", inserted as Boolean)
				if (stuff.container != originalStuff.container) {
					returnIntent.putExtra("newContainer", stuff.container)
				}
				setResult(RESULT_OK, returnIntent)
				finish()
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}

	override fun onBackPressed() {
		setResult(RESULT_CANCELED)
		super.onBackPressed()
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (requestCode == ContainerActivity.REQUEST.PICK.value) {
			if (resultCode == Activity.RESULT_OK) {
				val stuffID = data!!.getLongExtra("stuffID", -1L)
				if (stuffID != -1L) {
					stuff.container = stuffID
					binding.nameContainer.text = db.loadStuff(stuff.container).name
				}
			}
		}
	}
}